BEGIN
  FOR c IN ( SELECT object_name, object_type FROM user_objects WHERE object_type LIKE 'JAVA RESOURCE%' )
  LOOP
    DBMS_OUTPUT.put_line('DROP ' || c.object_type || ' ' || c.object_name);
    EXECUTE IMMEDIATE 'DROP ' || c.object_type || ' "' || c.object_name || '"';
  END LOOP;
  FOR c IN ( SELECT object_name, object_type FROM user_objects WHERE object_type LIKE 'JAVA SOURCE%' )
  LOOP
    DBMS_OUTPUT.put_line('DROP ' || c.object_type || ' ' || c.object_name);
    EXECUTE IMMEDIATE 'DROP ' || c.object_type || ' "' || c.object_name || '"';
  END LOOP;
  FOR c IN ( SELECT object_name, object_type FROM user_objects WHERE object_type LIKE 'JAVA CLASS%' )
  LOOP
    DBMS_OUTPUT.put_line('DROP ' || c.object_type || ' ' || c.object_name);
    EXECUTE IMMEDIATE 'DROP ' || c.object_type || ' "' || c.object_name || '"';
  END LOOP;
END;

SELECT object_name, object_type, status FROM user_objects WHERE object_type LIKE 'JAVA%';

SELECT CIFR('supersecreto') FROM DUAL;


