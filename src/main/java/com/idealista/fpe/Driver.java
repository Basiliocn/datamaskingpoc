package com.idealista.fpe;

import com.idealista.fpe.builder.FormatPreservingEncryptionBuilder;

public class Driver {
    static final byte[] key = hexStringToByteArray("6E327235753778214125442A472D4B61");
    static final byte[] tweak = hexStringToByteArray("782F413F4428472D");
    static FormatPreservingEncryption FPE = FormatPreservingEncryptionBuilder
        .ff1Implementation()
        .withDefaultDomain()
        .withDefaultPseudoRandomFunction(key)
        .withDefaultLengthRange()
        .build();

    public static String cifrar(String txt) throws Throwable {
        return FPE.encrypt(txt, tweak);
    }

    public static String descifrar(String txt) throws Throwable {
        return FPE.decrypt(txt, tweak);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
}
